# Wrap complexes
The notebook `wrap_complexes.ipynb` computes and shows Delaunay and Wrap complexes,
together with the associated discrete vector field.

<img src="https://i.imgur.com/eXqGOyQ.gif"></img>
